#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string>
#include <gtest/gtest.h>
#include "../include/TText.h"
#include "../src/TText.cpp"
#include "../include/TTextLink.h"
#include "../src/TTextLink.cpp"

TEST(TText, can_create_ttext) {
	ASSERT_NO_THROW(TText text);
}

TEST(TText, can_set_line) {
	TText text;
	string test = "TEST1";
	ASSERT_NO_THROW(text.SetLine(test));
}

TEST(TText, can_get_line) {
	TText text;
	string test = "TEST1";
	text.SetLine(test);
	EXPECT_EQ(text.GetLine(), test);
}

TEST(TText, can_go_next) {
	TText text;
	string test = "TEST1";
	text.InsNextLine(test);
	ASSERT_NO_THROW(text.GoNext());
	EXPECT_EQ(text.GetLine(), test);
}

TEST(TText, can_insert_right) {
	TText text;
	string test1 = "TEST1";
	string test2 = "TEST2";
	text.SetLine(test1);
	text.InsNextLine(test2);
	EXPECT_EQ(text.GetLine(), test1);
	text.GoNext();
	EXPECT_EQ(text.GetLine(), test2);
}

TEST(TText, can_delete) {
	TText text;
	string test1 = "TEST1";
	string test2 = "TEST2";
	text.SetLine(test1);
	text.InsNextLine(test2);
	text.DelNextLine();
	EXPECT_EQ(text.GoNext(), TextNoNext);
}

	

int main(int argc, char *argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
