#pragma once
#include "TDatValue.h"
#include "TTextLink.h"
#include <string>
#include <iostream>
#include "TDatacom.h"
#include <stack>

#define TextLineLength 30
#define MemSize 20

using namespace std;

class TText;
typedef TText *PTText;

class TText : public TDataCom {
protected:
	PTTextLink pFirst;      // ��������� ����� ������
	PTTextLink pCurrent;      // ��������� ������� ������
	stack< PTTextLink > Path; // ���� ���������� �������� �� ������
	stack< PTTextLink > St;   // ���� ��� ���������
	PTTextLink GetFirstAtom(PTTextLink pl); // ����� ������� �����
	void PrintText(PTTextLink ptl);         // ������ ������ �� ����� ptl
	PTTextLink ReadText(ifstream &TxtFile); //������ ������ �� �����
	static int TextLevel;
public:
	TText(PTTextLink pl = NULL);
	~TText() { pFirst = NULL; }
	PTText GetCopy();
	// ���������
	int GoFirstLink(void); // ������� � ������ ������
	int GoDownLink(void);  // ������� � ��������� ������ �� Down
	int GoNextLink(void);  // ������� � ��������� ������ �� Next
	int GoPrevLink(void);  // ������� � ���������� ������� � ������
												 // ������
	string GetLine(void);   // ������ ������� ������
	void SetLine(string s); // ������ ������� ������ 
													// �����������
	void InsDownLine(string s);    // ������� ������ � ����������
	void InsDownSection(string s); // ������� ������� � ����������
	void InsNextLine(string s);    // ������� ������ � ��� �� ������
	void InsNextSection(string s); // ������� ������� � ��� �� ������
	void DelDownLine(void);        // �������� ������ � ���������
	void DelDownSection(void);     // �������� ������� � ���������
	void DelNextLine(void);        // �������� ������ � ��� �� ������
	void DelNextSection(void);     // �������� ������� � ��� �� ������
																 // ��������
	int Reset(void);              // ���������� �� ������ �������
	int IsTextEnded(void) const;  // ����� ��������?
	int GoNext(void);             // ������� � ��������� ������
																//������ � �������
	void Read(string FileName);  // ���� ������ �� �����
	void Write(string FileName); // ����� ������ � ����
																//������
	void Print(void);             // ������ ������
	void PrintTextFile(PTTextLink ptl, ofstream& TxtFile);
};
