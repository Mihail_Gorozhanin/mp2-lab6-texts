#pragma once

#include "TText.h"
#include <iostream>
#include <string>
#include <conio.h>
#include <vector>

using namespace std;

class TTextViewer {
 public:
	 TTextViewer() { TTextLink::InitMemSystem(200); };
     TTextViewer(char* FilePath) {
         Text.Read(FilePath);
         string File(FilePath);
         this->FilePath = File;
     }
     TTextViewer(string FilePath) {
         Text.Read(FilePath.data());
         this->FilePath = FilePath;
     }
     TTextViewer(TText Text) {
         this->Text = Text;
     }
     void PushMessage(string message);
 private:
     TText Text;
     vector<string> arg;
     string FilePath = "";
     static int StrToInt(string str);
     void Help();
     void DeleteSection();
     void InsertLevel(string str);
     void Insert(string str);
     void Replace(string str);
     void Save();
     void Delete();
     int Navigation();
     vector<string> MessageToArgs(string message);
};