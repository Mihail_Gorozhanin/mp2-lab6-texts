# Методы программирования **2**
***
#Лабораторная работа №**6**: Тексты
***
##Введение:
***

>Обработка текстовой информации на компьютере широко применяется в различных областях человеческой деятельности: образование, наука, документооборот, кадровый и бухгалтерский учет и др. 
Вне зависимости от назначения текста типовыми операциями обработки являются создание, просмотр, редактирование и сохранение информации. В связи с тем, что объем текстовой информации может являться очень большим, для эффективного выполнения операций с ней необходимо выбрать представление текста, обеспечивающее структурирование и быстрый доступ к различным элементам текста. 

>Так, текст можно представить в виде линейной последовательности страниц, каждая из которых есть линейная последовательность строк, которые в свою очередь являются линейными последовательностями слов. Такое представление можно осуществлять с любой степенью детализации в зависимости от особенностей прикладной задачи. 

В рамках лабораторной работы рассматривается задача разработки учебного редактора текстов, в котором для представления данных используется *Иерархический связный список*. 

Подобная иерархическая структура представления может применяться при компьютерной реализации математических моделей в виде деревьев и, тем самым, может иметь самое широкое применение в самых различных областях приложений.

Таким образом лабораторная работа **"Тексты"** направлена на практическое освоение структуры данных **Иерархический связный список**.

***
##Основные понятия и определения
***

>**Текст** – это несколько предложений, связанных друг с другом по смыслу и грамматически. В рамках лабораторной работы в качестве примеров текстов рассматриваются тексты программ.

>**Редактор текстов** – программный комплекс, обеспечивающий выполнение операций обработки текста: создание, просмотр, редактирование и сохранение. Специализированные редакторы текстов могут поддерживать выполнение дополнительных операций (например, проверку синтаксиса или контекстный поиск).

>**Иерархический связный список** – это многосвязный список, в котором на каждое звено имеется ровно один указатель, а каждое звено содержит два указателя (один на следующее звено в том же уровне, другой на звено в нижерасположенном уровне).

***
##Требования к лабораторной работе
***
В рамках лабораторной работы ставится задача разработки учебного редактора текстов с поддержкой следующих операций:

- выбор текста для редактирования (или создание нового текста);
- демонстрация текста на экране дисплея;
- поддержка средств указания элементов (уровней) текста;
- вставка, удаление и замена строк текста;
- запись подготовленного текста в файл.

>При выполнении операций чтения (при выборе для редактирования уже существующего текста) и записи редактор должен использовать стандартный формат, принятый в файловой системе для представления текстовых файлов, обеспечивая тем самым совместимость учебного редактора текстов и существующего программного обеспечения.

***
##Условия и ограничения
***
В рамках выполнения данной лабораторной работы могут быть использованы следующие основные допущения:

- При планировании структуры текста в качестве самого нижнего уровня можно рассматривать уровень строк.
- В качестве тестовых текстов можно рассматривать текстовые файлы программы.

***
##Методы выполнения лабораторной работы
***

###Структуры данных

При выборе модели представления текста необходимо учитывать, какие операции будут выполняться. Можно рассмотреть несколько моделей представления текста: текст можно рассматривать как линейную последовательность символов, как линейную последовательность строк, как линейную последовательность страниц и т.д.

>Единство возможных моделей текста можно обеспечить, если при введении каждого более высокого уровня использовать все ранее введенные (ниже расположенные) структуры представления. Такой подход может быть реализован с использованием иерархических связных списков. Звенья такого списка содержат по два указателя: один используется для организации отношения следования по элементам текста одного и того же уровня, а другой – для указания отношения следования по иерархии уровней представления текста (от более высокого уровня к ниже расположенному).

Необходимым условием применения связного списка как структуры хранения текста является возможность нахождения такого унифицированного типа звеньев, который можно было бы использовать на любых уровнях представления текста, так как реализация своего типа звеньев для каждого уровня приведет к увеличению трудоемкости управления памятью и дублированию программ обработки. Возможное решение при выборе структуры звеньев может состоять в следующем:

- каждое звено структуры хранения содержит два поля указателей и поле для хранения данных;
- нижний уровень иерархической структуры звеньев ограничивается уровнем строк, а не символов. Это повышает эффективность использования памяти, так как в случае отдельного уровня для хранения символов, затраты на хранение служебной информации (указатели) в несколько раз превышают затраты на хранение самих данных.
- при использовании строк в качестве атомарных элементов поле для хранения данных является массивом символов заданного размера;
- количество уровней и количество звеньев на каждом уровне может быть произвольным;
- при использовании звена на атомарном уровне поле указателя на нижний уровень структуры устанавливается равным значению NULL;
- при использовании звена на структурных уровнях представления текста поле для хранения строки текста используется для размещения наименования соответствующего элемента текста;
- количество используемых уровней представления в разных фрагментах текста может быть различным.

При выбранном способе представления для следующего примера текста:
>Раздел 2
>1.1. Полиномы

>      1. Определение 
>      2. Структура
1.2. Тексты Определение Структура

>     1. Определение
>     2. Структура

структура хранения текста будет иметь вид:
![](images/1.png) 

***
##Алгоритмы
***

Для индикации позиции в тексте, относительно которой выполняются операции перемещения по тексту, вводится понятие текущей строки. Для перемещения по тексту предлагается реализовать следующие операции:

- переход к первой строке текста;
- переход к следующему элементу в том же уровне;
- переход к элементу в нижерасположенном уровне;
- переход к предыдущей позиции в тексте.

Разработанная структура хранения текста обеспечивает возможность передвижения по структуре только в направлении следующих или нижерасположенных элементов. Движение по элементам текста в обратном направлении возможно только при условии, если адреса звеньев, на которые необходимо переместиться, были каким-либо способом запомнены ранее. Возможный способ организации запоминания этих адресов состоит в использовании стека.

Необходимо реализовать операции вставки и удаления элементов текста. Операции должны применяться для текущего и нижерасположенного уровней. Для операции вставки необходимо разработать вариант образования новых подуровней – так, например, выполнение операции вставки для подуровня может быть представлено следующим образом

![](images/2.png) 

Для операции удаления должны быть предусмотрены варианты удаления как отдельных строк, так и всего содержимого подуровней текста.

###Обход текста
Для последовательного доступа ко всем элемента текста (например, для печати текста) необходимо совершить обход текста.

Схема обхода может состоять в следующем. При начале обхода следует перейти от начала текста (корня дерева) до атомарного уровня по указателям нижерасположенных уровней, запоминая при этом все пройденные звенья в стеке. После обработки найденной строки, далее следует переходить по строкам того же уровня, до последней строки в этом уровне. После обхода уровня текущего уровня необходимо извлечь звено из стека и повторить всю выше приведенную последовательность действий. Обход текста будет завершен, когда стек пуст. Данная схема обхода представляет вариант TDN (top – down – next, вершина ‑ нижний уровень – следующий элемент).

При запоминании иерархически представленного текста в текстовом файла необходимо обеспечить запоминание структуры текста. Возможный вариант состоит в использовании специальных символов для выделения моментов начала и завершения уровней текста (в качестве таких служебных символов можно использовать, например, знаки фигурных скобок «{» и «}»).

###Итератор

Как и при работе с линейными списками, использование итератора позволяет упростить реализацию операций с текстом и обеспечивает унифицированный способ обработки элементов структуры данных.
Итератор включет следующие методы:

- инициализация (установка на корневое звено)

В этом методе происходит опустошение стека, после чего в стек помещаются указатель на корневое звено, указатели на следующее звено и следующее в подуровне.

- проверка завершения текста

Если стек пуст, то текст завершен.

- переход к следующему звену

В стек помещаются указатели на следующее звено и на звено, следующее в подуровне.

###Копирование

Для копирования текста необходимо осуществить обход текста. Так как структура текста является нелинейной, то копирование производится за два прохода, при этом для навигации по исходному тексту и тексту копии используется один объединенный стек.

Первый проход производится при подъеме на строку из подуровня – для текущей строки выполняется:
создание копии звена;

- заполнение в звене-копии поля указателя подуровня pDown (подуровень уже скопирован);
- запись в звене-копии в поле данных значения “Copy”, используемое как маркер для распознавания звена при попадании на него при втором проходе; предполагается, что в тексте данный маркер не встречается;
- запись в звене-копии в поле указателя следующего звена pNext указателя на звено-оригинал (для возможности последующего копирования текста исходной строки);
- запись указателя на звено-копию в стек. 

Второй проход производится при извлечении звена-копии из стека (распознается по маркеру “Copy”)– в этом случае необходимо выполнить:

- заполнение в звене-копии полей данных и указателя следующего звена;
- указатель на звено-копию запоминается в служебной переменной.

###Сборка мусора

При удалении разделов текста для освобождения звеньев следует учитывать следующие моменты:

- обход всех звеньев удаляемого текста может потребовать длительного времени;
- при допущении множественности ссылок на разделы текста (для устранения дублирования одинаковых частей) удаляемый текст нельзя исключить, так как он может быть задействован в других фрагментах текста.

Для решения этих проблем можно не освобождать память в момент удаления, а фиксировать удаление текста установкой соответствующего указателя в состояние NULL. При такой реализации операции удаления может возникнуть ситуация, когда в памяти, отведенной под хранение текста, будут присутствовать звенья, на которые нет указателей в тексте. Таким образом, эти звенья не могут быть возвращены системе для последующего использования, а память считается занятой (возникает так называемая утечка памяти). Такие звенья называются «мусором».

>Наличие «мусора» в системе может быть допустимым, если имеющейся свободной памяти достаточно для работы программ. В случае нехватки памяти необходимо выполнить «сборку мусора» (garbage collection).

Для осуществления данного подхода необходимо программно реализовать систему управления памятью для представления текстов. Для данной системы управления память должна выделяться полностью при начале работы программы, размер выделяемой памяти может определяться как параметр системы. Для фиксации состояния памяти в классе TTextLink может быть определена статическая переменная 
```
MemHeader класс
TTextMem:
class TTextMem {
  PTTextLink pFirst;      // первое звено
  PTTextLink pLast;       // последнее звено 
  PTTextLink pFree;       // первое свободное звено 
};
```
Для начального выделения и форматирования памяти используется статический метод InitMemSystem класса TTextLink. В этом методе выделяется память для хранения текстов. Указатель pFirst устанавливается на начало этого массива (после приведения типа к типу указателя на звено), указатель pLast на последний элемент массива. Далее этот массив размечается как список свободных звеньев, в самом начале работы список свободных звеньев может быть упорядочен по памяти.

Для выделения памяти под звено перегружается оператор new, который выделяет новое звено из списка свободных звеньев. При освобождении звена в перегруженном операторе delete происходит возвращение памяти в список свободных звеньев.

Алгоритм «сборки мусора» состоит из трех этапов. 
>На **первом этапе** происходит обход текста и маркирование звеньев текста специальными символами (например, «&&&»). 

>На **втором этапе** происходит обход и маркирование списка свободных звеньев. 

>На **третьем этапе** происходит проход по всему непрерывному участку памяти как по непрерывному набору звеньев. Если звено промаркировано, то маркер снимается. В противном случае, найдено звено, на которое нет ссылок («мусор»), и это звено возвращается в список свободных звеньев.



***
##Разработка программного комплекса
***

###Структура проекта

С учетом сформулированных выше предложений к реализации целесообразной представляется следующая модульная структура программы:

- TTextLink.h, TTextLink.cpp – модуль с классом для звена текста;
- TText.h, TText.cpp – модуль с классом, реализующим операции над текстом;
- TTextViewer.h, TTextViewer.cpp – модуль с классом, реализующим визуализацию текста;

![](images/3.png)

### Цели работы:   


##### В рамках лабораторной работы ставится задача разработки учебного редактора текстов, основанного на структуре хранения *Иерархический связный список*, с поддержкой следующих операций: 

- выбор текста для редактирования (или создание нового текста); 
- демонстрация текста на экране дисплея;
- вставка, удаление и замена строк текста; 
- запись подготовленного текста в файл.*
 
***


### Задачи:  
1. Разработка интерфейса всех необходимых классов перечисленных в пункте **Структура проекта**. (Доработка уже готовых и разработка пустых)
2. Реализация методов всех необходимых классов согласно заданному интерфейсу.(см. пункт **Структура проекта**)
3. Разработка тестов для проверки работоспособности текстового редактора.
4. Реализация алгоритма работы с текстами.
5. Реализация операций для работы с текстами.
6. Обеспечение работоспособности тестов и примера использования.
***

### Используемые инструменты:

***
- Система контроля версий Git. 
- Фреймворк для написания автоматических тестов Google Test.
- Среда разработки Microsoft Visual Studio (2017).
***


## Начало работы:

***
### 1. Разработка и реализация класса `TDatacom`:
***

#### Объявление и реализация (h-файл):
```
#pragma once

#define TextOK	0 // ошибок нет
// коды ситуаций
#define TextNoDown 101 // нет подуровня для текущей позиции
#define TextNoNext 102 // нет следующего раздела текущего уровня
#define TextNoPrev 103 // текущая позиция в начале текста
// коды ошибок
#define TextError -102 // ошибка в тексте
#define TextNoMem -101 // нет памяти

class TDataCom
{
protected:
	int RetCode;

	int SetRetCode(int ret) { return RetCode = ret; }
public:
	TDataCom() : RetCode(TextOK) {}
	virtual ~TDataCom() = 0 {}
	int GetRetCode()
	{
		int temp = RetCode;
		RetCode = TextOK;
		return temp;
	}
};

```
***
### 2. Разработка и реализация абстрактного класса объектов-значений `TDatValue`:
***
#### Объявление и реализация (h-файл):
```
// DatValue.h
// модуль, объявляющий абстрактный класс объектов-значений списка

#pragma once

class TDatValue;
typedef TDatValue *PTDatValue;

class TDatValue {
public:
	virtual TDatValue * GetCopy() = 0; // создание копии
	~TDatValue() {}
};
```

***
### 3. Разработка и реализация базового класса для звеньев (элементов) списка `TTextLink` :
***
#### Объявление (h-файл):
```
#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include <string>
#include "TDatValue.h"
#include <iostream>

#define TextLineLength 255
#define MemSize 20

using namespace std;

class TText;
class TTextLink;
typedef TTextLink *PTTextLink;
typedef char TStr[TextLineLength];


class TTextMem {
	PTTextLink pFirst;     // указатель на первое звено
	PTTextLink pLast;      // указатель на последнее звено
	PTTextLink pFree;      // указатель на первое свободное звено
	friend class TTextLink;
};
typedef TTextMem *PTTextMem;

class TTextLink : public TDatValue {
protected:
	TStr Str;  // поле для хранения строки текста
	PTTextLink pNext, pDown;  // указатели по тек. уровень и на подуровень
	static TTextMem MemHeader; // система управления памятью
public:
	static void InitMemSystem(int size = MemSize); // инициализация памяти
	static void PrintFreeLink(void);  // печать свободных звеньев
  void * operator new (size_t size); // выделение звена
	void operator delete (void *pM);   // освобождение звена
	static void MemCleaner(TText &txt); // сборка мусора
	TTextLink(TStr s = NULL, PTTextLink pn = NULL, PTTextLink pd = NULL) {
		pNext = pn; pDown = pd;
		if (s != NULL) strcpy(Str, s); else Str[0] = '\0';
	}
	~TTextLink() {}
	int IsAtom() { return pDown == NULL; } // проверка атомарности звена
	PTTextLink GetNext() { return pNext; }
	PTTextLink GetDown() { return pDown; }
	PTDatValue GetCopy() { return new TTextLink(Str, pNext, pDown); }
protected:
	virtual void Print(ostream &os) { os << Str; }
	friend class TText;
	TTextLink(string s) {
		pNext = nullptr; pDown = nullptr;
		strcpy_s(Str, s.c_str());
	}
};
```
#### Реализация (cpp-файл):
```
#pragma once
#include "../include/TTextLink.h"
#include "../include/TText.h"

TTextMem TTextLink::MemHeader;

void TTextLink::InitMemSystem(int size) {
	char *tmp = new char[sizeof(TTextLink)*size];
	MemHeader.pFirst = (PTTextLink)tmp;
	MemHeader.pFree = (PTTextLink)tmp;
	MemHeader.pLast = (PTTextLink)tmp + size - 1;
	PTTextLink pLink = MemHeader.pFirst;

	for (int i = 0; i < size - 1; i++, pLink++)
		pLink->pNext = pLink + 1;
	pLink->pNext = NULL;
}

void TTextLink::PrintFreeLink(void) {
	PTTextLink pLink = MemHeader.pFree;
	for (; pLink != NULL; pLink = pLink->pNext)
		cout << pLink->Str << endl;
}

void * TTextLink:: operator new (size_t size) {
	PTTextLink pLink = MemHeader.pFree;
	if (MemHeader.pFree != NULL) MemHeader.pFree = pLink->pNext;
	return pLink;
}

void TTextLink:: operator delete (void *pM) {
	PTTextLink pLink = (PTTextLink)pM;
	pLink->pNext = MemHeader.pFree;
	MemHeader.pFree = pLink;
}

void TTextLink::MemCleaner(TText &txt) {
	for (txt.Reset(); !txt.IsTextEnded(); txt.GoNext()) {
		txt.SetLine(txt.GetLine());
	}

	PTTextLink pLink = MemHeader.pFree;
	for (; pLink != NULL; pLink = pLink->pNext)
		strcpy(pLink->Str, "A");

	pLink = MemHeader.pFirst;
	for (; pLink <= MemHeader.pLast; pLink++) {
		if (strstr(pLink->Str, "A") != NULL) {
			strcpy(pLink->Str, pLink->Str + 3);
		}
		else delete pLink;
	}
}
```

***
### 4. Разработка и реализация класса `TText`:
***

#### Объявление (h-файл):
```
#pragma once
#include "TDatValue.h"
#include "TTextLink.h"
#include <string>
#include <iostream>
#include "TDatacom.h"
#include <stack>

#define TextLineLength 30
#define MemSize 20

using namespace std;

class TText;
typedef TText *PTText;

class TText : public TDataCom {
protected:
	PTTextLink pFirst;      // указатель корня дерева
	PTTextLink pCurrent;      // указатель текущей строки
	stack< PTTextLink > Path; // стек траектории движения по тексту
	stack< PTTextLink > St;   // стек для итератора
	PTTextLink GetFirstAtom(PTTextLink pl); // поиск первого атома
	void PrintText(PTTextLink ptl);         // печать текста со звена ptl
	PTTextLink ReadText(ifstream &TxtFile); //чтение текста из файла
	static int TextLevel;
public:
	TText(PTTextLink pl = NULL);
	~TText() { pFirst = NULL; }
	PTText GetCopy();
	// навигация
	int GoFirstLink(void); // переход к первой строке
	int GoDownLink(void);  // переход к следующей строке по Down
	int GoNextLink(void);  // переход к следующей строке по Next
	int GoPrevLink(void);  // переход к предыдущей позиции в тексте
												 // доступ
	string GetLine(void);   // чтение текущей строки
	void SetLine(string s); // замена текущей строки 
													// модификация
	void InsDownLine(string s);    // вставка строки в подуровень
	void InsDownSection(string s); // вставка раздела в подуровень
	void InsNextLine(string s);    // вставка строки в том же уровне
	void InsNextSection(string s); // вставка раздела в том же уровне
	void DelDownLine(void);        // удаление строки в подуровне
	void DelDownSection(void);     // удаление раздела в подуровне
	void DelNextLine(void);        // удаление строки в том же уровне
	void DelNextSection(void);     // удаление раздела в том же уровне
																 // итератор
	int Reset(void);              // установить на первую звапись
	int IsTextEnded(void) const;  // текст завершен?
	int GoNext(void);             // переход к следующей записи
																//работа с файлами
	void Read(string FileName);  // ввод текста из файла
	void Write(string FileName); // вывод текста в файл
																//печать
	void Print(void);             // печать текста
	void PrintTextFile(PTTextLink ptl, ofstream& TxtFile);
};

```
#### Реализация (cpp-файл):
```
#pragma once
#include "../include/TText.h"
#include <string>
#include <iostream>
#include <fstream>

const int buf = 80;
int TText::TextLevel;

using namespace std;

//конструктор
TText::TText(PTTextLink pl) {
	if (pl == NULL) pl = new TTextLink();
	pFirst = pl;
}

//навигация
int TText::GoFirstLink(void) {
	while (!Path.empty()) Path.pop();
	pCurrent = pFirst;
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else
		SetRetCode(TextOK);
	return RetCode;
}

int TText::GoDownLink(void) {
	SetRetCode(TextNoDown);
	if (pCurrent != NULL)
		if (pCurrent->pDown != NULL) {
			Path.push(pCurrent);
			pCurrent = pCurrent->pDown;
			SetRetCode(TextOK);
		}
	return RetCode;
}

int TText::GoNextLink(void) {
	SetRetCode(TextNoNext);
	if (pCurrent != NULL)
		if (pCurrent->pNext != NULL) {
			Path.push(pCurrent);
			pCurrent = pCurrent->pNext;
			SetRetCode(TextOK);
		}
	return RetCode;
}

int TText::GoPrevLink(void) {
	if (Path.empty())
		SetRetCode(TextNoPrev);
	else {
		pCurrent = Path.top();
		Path.pop();
		SetRetCode(TextOK);
	}
	return RetCode;
}

//доступ
string TText::GetLine(void) {
	if (pCurrent == NULL)
		return string("");
	else
		return string(pCurrent->Str);
}

void TText::SetLine(string s) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else
		strcpy_s(pCurrent->Str, s.c_str());
}

//модификация
void TText::InsDownLine(string s) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else {
		TStr buf;
		strcpy_s(buf, s.c_str());
		pCurrent->pDown = new TTextLink(buf, pCurrent->pDown, NULL);
		SetRetCode(TextOK);
	}
}

void TText::InsDownSection(string s) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else {
		TStr buf;
		strcpy_s(buf, s.c_str());
		pCurrent->pDown = new TTextLink(buf, NULL, pCurrent->pDown);
		SetRetCode(TextOK);
	}
}

void TText::InsNextLine(string s) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else {
		TStr buf;
		strcpy_s(buf, s.c_str());
		pCurrent->pNext = new TTextLink(buf, pCurrent->pNext, NULL);
		SetRetCode(TextOK);
	}
}
void TText::InsNextSection(string s) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else {
		TStr buf;
		strcpy_s(buf, s.c_str());
		pCurrent->pNext = new TTextLink(buf, NULL, pCurrent->pNext);
		SetRetCode(TextOK);
	}
}

void TText::DelDownLine(void) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else if (pCurrent->pDown == NULL)
		SetRetCode(TextNoDown);
	else if (pCurrent->pDown->IsAtom())
		pCurrent->pDown = pCurrent->pDown->pNext;
}

void TText::DelDownSection(void) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else if (pCurrent->pDown == NULL)
		SetRetCode(TextNoDown);
	else
		pCurrent->pDown = NULL;
}

void TText::DelNextLine(void) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else if (pCurrent->pNext == NULL)
		SetRetCode(TextNoNext);
	else if (pCurrent->pNext->IsAtom())
		pCurrent->pNext = pCurrent->pNext->pNext;
}

void TText::DelNextSection(void) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else if (pCurrent->pNext == NULL)
		SetRetCode(TextNoNext);
	else
		pCurrent->pNext = pCurrent->pNext->pNext;
}

//итератор
int TText::Reset(void) {
	while (!St.empty())
		St.pop();
	pCurrent = pFirst;
	if (pCurrent != NULL) {
		St.push(pCurrent);
		if (pCurrent->pNext != NULL)
			St.push(pCurrent->pNext);
		if (pCurrent->pDown != NULL)
			St.push(pCurrent->pDown);
	}
	return IsTextEnded();
}

int TText::IsTextEnded(void) const {
	return !St.size();
}

int TText::GoNext(void) {
	if (!IsTextEnded()) {
		pCurrent = St.top(); St.pop();
		if (pCurrent != pFirst) {
			if (pCurrent->pNext != NULL)
				St.push(pCurrent->pNext);
			if (pCurrent->pDown != NULL)
				St.push(pCurrent->pDown);
		}
	}
	return IsTextEnded();
}

//копирование текста
PTTextLink TText::GetFirstAtom(PTTextLink pl) {
	PTTextLink tmp = pl;
	while (!tmp->IsAtom()) {
		St.push(tmp);
		tmp = tmp->GetDown();
	}
	return tmp;
}

//печать
void TText::Print() {
	TextLevel = 0;
	PrintText(pFirst);
}

void TText::PrintText(PTTextLink ptl) {
	if (ptl != NULL) {
		for (int i = 0; i < TextLevel; i++)
			cout << " ";
		cout << ptl->Str << endl;
		TextLevel++; PrintText(ptl->GetDown());
		TextLevel--; PrintText(ptl->GetNext());
	}
}

void TText::PrintTextFile(PTTextLink ptl, ofstream & TxtFile)
{
	if (ptl != NULL) {
		for (int i = 0; i < TextLevel; i++)
			TxtFile << " ";
		TxtFile << ptl->Str << endl;
		TextLevel++; PrintTextFile(ptl->GetDown(), TxtFile);
		TextLevel--; PrintTextFile(ptl->GetNext(), TxtFile);
	}
}

PTText TText::GetCopy()
{
	PTTextLink pl1, pl2 = NULL, pl = pFirst, cpl = NULL;

	if (pFirst != NULL) {
		while (!St.empty()) St.pop();
		while (true)
		{
			if (pl != NULL) {									// Переход к первому атому
				pl = GetFirstAtom(pl);
				St.push(pl);
				pl = pl->GetDown();
			}
			else if (St.empty()) break;
			else {
				pl1 = St.top(); St.pop();
				if (strstr(pl1->Str, "Copy") == NULL) {			// первый этап, создание копии pDown на уже скопированный подуровень
					pl2 = new TTextLink("Copy", pl1, cpl);
					St.push(pl2);
					pl = pl1->GetNext();
					cpl = NULL;
				}
				else {											//Второй этап
					strcpy_s(pl1->Str, pl1->pNext->Str);
					pl1->pNext = cpl;
					cpl = pl1;
				}
			}
		}
	}

	return new TText(cpl);
}


//чтение
void TText::Read(string FileName) {
	ifstream TxtFile(FileName);
	TextLevel = 0;
    if (TxtFile.is_open())
        pFirst = ReadText(TxtFile);
    else
        throw "Cant open the file";
}

PTTextLink TText::ReadText(ifstream& TxtFile)
{
	string buf;
	PTTextLink ptl = new TTextLink();
	PTTextLink tmp = ptl;
	while (!TxtFile.eof())
	{
		getline(TxtFile, buf);
		if (buf.size() > 0 && buf.front() == '}')
			break;
		else if (buf.size() > 0 && buf.front() == '{')
			ptl->pDown = ReadText(TxtFile);
		else
		{
			ptl->pNext = new TTextLink(buf.c_str());
			ptl = ptl->pNext;
		}
	}
	ptl = tmp;
	if (tmp->pDown == NULL)
	{
		tmp = tmp->pNext;
		delete ptl;
	}
	return tmp;
}

void TText::Write(string FileName)
{
	TextLevel = 0;
	ofstream TextFile(FileName);
	PrintTextFile(pFirst, TextFile);
}
```

***
### 5. Разработка и реализация класса текстового редактора `TTextViewer`:
***
#### Объявление (h-файл):
```
#pragma once

#include "TText.h"
#include <iostream>
#include <string>
#include <conio.h>
#include <vector>

using namespace std;

class TTextViewer {
 public:
	 TTextViewer() { TTextLink::InitMemSystem(200); };
     TTextViewer(char* FilePath) {
         Text.Read(FilePath);
         string File(FilePath);
         this->FilePath = File;
     }
     TTextViewer(string FilePath) {
         Text.Read(FilePath.data());
         this->FilePath = FilePath;
     }
     TTextViewer(TText Text) {
         this->Text = Text;
     }
     void PushMessage(string message);
 private:
     TText Text;
     vector<string> arg;
     string FilePath = "";
     static int StrToInt(string str);
     void Help();
     void DeleteSection();
     void InsertLevel(string str);
     void Insert(string str);
     void Replace(string str);
     void Save();
     void Delete();
     int Navigation();
     vector<string> MessageToArgs(string message);
};
```
#### Реализация (cpp-файл):
```
#pragma once

#include "../include/TTextViewer.h"

void TTextViewer::PushMessage(string message) {

    arg = MessageToArgs(message);

    if (arg.size() > 0 && arg[0] == "quit" || arg[0] == "q" || arg[0] == "exit")
        cout << "goodbye" << endl;
    else if (arg.size() > 0 && arg[0] == "help")
        Help();
    else if (arg.size() > 0 && arg[0] == "save" && arg.size() == 2) {
        Text.Write(arg[1]);
        FilePath = arg[1];
    }
    else if (arg.size() > 0 && arg[0] == "save" && arg.size() == 1)
        Save();
    else if (arg.size() > 0 && arg[0] == "load" && arg.size() == 2) {
        Text.Read(arg[1]);
        FilePath = arg[1];
    }
    else if (arg.size() > 0 && arg[0] == "insert" && arg.size() >= 3)
        Insert(arg[arg.size() - 1]);
    else if (arg.size() > 0 && arg[0] == "insertsection" && arg.size() >= 3)
        InsertLevel(arg[arg.size() - 1]);
    else if (arg.size() > 0 && arg[0] == "deletesection" && arg.size() >= 2)
        DeleteSection();
    else if (arg.size() > 0 && arg[0] == "replace" && arg.size() >= 3)
        Replace(arg[arg.size() - 1]);
    else if (arg.size() > 0 && arg[0] == "delete" && arg.size() >= 2)
        Delete();
    else if (arg.size() > 0 && arg[0] == "show")
        Text.Print();
    else if (arg.size() == 0)
        throw "you dont write any command";
    else
        throw "incorrect command or arguments, write help to show abilities of programm";

}

void TTextViewer::Insert(string str) {
    Navigation();
    Text.InsNextLine(str);
}


void TTextViewer::Replace(string str) {
    Navigation();
    Text.SetLine(str);
}

void TTextViewer::Delete() {
    Navigation();
    Text.GoPrevLink();
    Text.DelNextLine();
}

void TTextViewer::InsertLevel(string str) {
    Navigation();
    Text.InsDownSection(str);
}

void TTextViewer::DeleteSection() {
    Navigation();
    Text.DelDownSection();
}

void TTextViewer::Save() {
    if (FilePath != "")
        Text.Write(FilePath.data());
    else
        throw "where to save?";
}

int TTextViewer::Navigation() {
    Text.Reset();

    for (int i = 1; i < arg.size() - 1; i++) {
        for (int f = 0; f < StrToInt(arg[i]); f++) {
            if (Text.GoNextLink())
                throw "error of navigation";
        }
        if ((i + 1) != arg.size() - 1)
            if (Text.GoDownLink())
                throw "error of navigation";
    }
}

vector<string> TTextViewer::MessageToArgs(string message) {
    vector<string> result;
    bool IsIntoString = false;
    string buffstring = "";

    for (int i = 0; i < message.size(); i++) {
        if (message.at(i) == ' ' && !IsIntoString && buffstring.size() > 0) {
            result.push_back(buffstring);
            buffstring.clear();
        }
        else if (message.at(i) == '"')
            IsIntoString = !IsIntoString;
        else if (message.at(i) != ' ' || IsIntoString)
            buffstring.push_back(message.at(i));
    }
    if (buffstring.size() > 0)
        result.push_back(buffstring);

    return result;
}

int TTextViewer::StrToInt(string str) {
    int result = 0;

    for (int i = str.size() - 1, f = 0; i >= 0; i--, f++)
        if (str.at(i) <= '9' && str.at(i) >= '0')
            result = (str.at(i) - '0') * pow(10, f);
        else
            throw ("error: incorrect char of one of argument: " + str.at(i));

    return result;
}

void TTextViewer::Help() {
    cout << "commands: " << endl;
    cout << "save, save \"path\", load \"path\" - save or load text" << endl;
    cout << "<NavigationArguments> means arguments of the form: <NumOfString1> <NumOfString2> ... <NumOfStringN>" << endl;
    cout << "<NumOfString1> is mandatory argument, and other is optional arguments" << endl;
    cout << "if you write more than one <NumOfString> argument, you mean down to section" << endl;
    cout << "insert/replace <NavigationArguments> \"string\" - insert or replace string" << endl;
    cout << "insertsection <NumOfString> \"string\" - insert level with string" << endl;
    cout << "delete <NumOfString> - delete string" << endl;
    cout << "show - print text on console" << endl;
    cout << "exit, quit, q - quit the program" << endl;
    cout << "Credits: " << endl;
    cout << "Text editor by Aleksandr Nazarov and Mihail Gorozhanin" << endl;
}
```

***
### 6.Разработка и реализация тестовой программы `Main`:
***
#### Реализация (cpp-файл):
```
#pragma once

#include "../include/TTextViewer.h"

int main() {
    TTextViewer Viewer;
    string command = "";
    while (command != "quit" && command != "q" && command != "exit") {
        try {
            command = "";
            getline(cin, command);
            Viewer.PushMessage(command);
        }
        catch (char* err) {
            cout << err << endl;
        }
        catch (string err) {
            cout << err << endl;
        }
    }
    return 0;
}
		
```

***
###7. Тесты для проверки  **TText** и **TTextLink** (GoogleTestFramework)
***
#####**Test_gtest.cpp**
```c++
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string>
#include <gtest/gtest.h>
#include "../include/TText.h"
#include "../src/TText.cpp"
#include "../include/TTextLink.h"
#include "../src/TTextLink.cpp"

TEST(TText, can_create_ttext) {
	ASSERT_NO_THROW(TText text);
}

TEST(TText, can_set_line) {
	TText text;
	string test = "TEST1";
	ASSERT_NO_THROW(text.SetLine(test));
}

TEST(TText, can_get_line) {
	TText text;
	string test = "TEST1";
	text.SetLine(test);
	EXPECT_EQ(text.GetLine(), test);
}

TEST(TText, can_go_next) {
	TText text;
	string test = "TEST1";
	text.InsNextLine(test);
	ASSERT_NO_THROW(text.GoNext());
	EXPECT_EQ(text.GetLine(), test);
}

TEST(TText, can_insert_right) {
	TText text;
	string test1 = "TEST1";
	string test2 = "TEST2";
	text.SetLine(test1);
	text.InsNextLine(test2);
	EXPECT_EQ(text.GetLine(), test1);
	text.GoNext();
	EXPECT_EQ(text.GetLine(), test2);
}

TEST(TText, can_delete) {
	TText text;
	string test1 = "TEST1";
	string test2 = "TEST2";
	text.SetLine(test1);
	text.InsNextLine(test2);
	text.DelNextLine();
	EXPECT_EQ(text.GoNext(), TextNoNext);
}

	

int main(int argc, char *argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

```

***
### 8. Обеспечение работоспособности тестов:
***
![](images/Test.png)
***
###9. Результат работы программы:
![](images/ApplicationTest0.png)
![](images/ApplicationTest1.png)
![](images/ApplicationTest2.png)
***
### 10. Анализ результата
***
Анализ показывает, что результаты программа выдает верно и правильно выполняет поставленные задачи, к тому же в данной программе (текстовом редакторе) были реализованы дополнительные возможности по работе с текстами. 

***
## Вывод:
***
##### Все цели и задачи работы достигнуты. В ходе выполнения данной работы, нам удалось разработать и реализовать несколько классов:

- класс `TDatValue` для определения класса объектов-значений списка (абстрактный класс);
- класс `TTextLink` для определения звеньев (элементов) текстов;
- класс `TText` для реализации функций работы с текстами;
- класс `TTextViewer` для обеспечения удобной работы с текстами пользователю;

##### Также были написаны тесты к данному проекту под систему тестирования `Google Test`, что позволяет не только проверить правильность работы программы, но и также лучше ознакомиться с `Google Test Framework`. Успешное же прохождение всех тестов показывает, что классы были реализованы корректно и программа созданная с использованием этих классов работает верно. 
##### Особенностью данной работы было то, что:
1. Тесты для проверки корректности работы были написаны полностью самостоятельно, и в связи с большим количеством методов работы были сосредоточены на качественной проверке их правильности и работоспособности в небольшом количестве.
2. Была освоена структура **"Иерархических связных списков"**, которая может быть использована в дальнейших работах.
3. Реализация данной структуры была осуществлена изначально без использования предыдущего опыта(за исключением `TDatValue` и переработанного `tdatacom`)
4. Также на основе данной структуры была создана программа для работы с текстами. 
5. Работа проходила в командном виде (над одним "проектом" работали двое).
