#pragma once

#include "../include/TTextViewer.h"

int main() {
    TTextViewer Viewer;
    string command = "";
    while (command != "quit" && command != "q" && command != "exit") {
        try {
            command = "";
            getline(cin, command);
            Viewer.PushMessage(command);
        }
        catch (char* err) {
            cout << err << endl;
        }
        catch (string err) {
            cout << err << endl;
        }
    }
    return 0;
}
