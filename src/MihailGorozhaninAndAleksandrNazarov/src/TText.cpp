#pragma once
#include "../include/TText.h"
#include <string>
#include <iostream>
#include <fstream>

const int buf = 80;
int TText::TextLevel;

using namespace std;

//�����������
TText::TText(PTTextLink pl) {
	if (pl == NULL) pl = new TTextLink();
	pFirst = pl;
}

//���������
int TText::GoFirstLink(void) {
	while (!Path.empty()) Path.pop();
	pCurrent = pFirst;
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else
		SetRetCode(TextOK);
	return RetCode;
}

int TText::GoDownLink(void) {
	SetRetCode(TextNoDown);
	if (pCurrent != NULL)
		if (pCurrent->pDown != NULL) {
			Path.push(pCurrent);
			pCurrent = pCurrent->pDown;
			SetRetCode(TextOK);
		}
	return RetCode;
}

int TText::GoNextLink(void) {
	SetRetCode(TextNoNext);
	if (pCurrent != NULL)
		if (pCurrent->pNext != NULL) {
			Path.push(pCurrent);
			pCurrent = pCurrent->pNext;
			SetRetCode(TextOK);
		}
	return RetCode;
}

int TText::GoPrevLink(void) {
	if (Path.empty())
		SetRetCode(TextNoPrev);
	else {
		pCurrent = Path.top();
		Path.pop();
		SetRetCode(TextOK);
	}
	return RetCode;
}

//������
string TText::GetLine(void) {
	if (pCurrent == NULL)
		return string("");
	else
		return string(pCurrent->Str);
}

void TText::SetLine(string s) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else
		strcpy_s(pCurrent->Str, s.c_str());
}

//�����������
void TText::InsDownLine(string s) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else {
		TStr buf;
		strcpy_s(buf, s.c_str());
		pCurrent->pDown = new TTextLink(buf, pCurrent->pDown, NULL);
		SetRetCode(TextOK);
	}
}

void TText::InsDownSection(string s) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else {
		TStr buf;
		strcpy_s(buf, s.c_str());
		pCurrent->pDown = new TTextLink(buf, NULL, pCurrent->pDown);
		SetRetCode(TextOK);
	}
}

void TText::InsNextLine(string s) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else {
		TStr buf;
		strcpy_s(buf, s.c_str());
		pCurrent->pNext = new TTextLink(buf, pCurrent->pNext, NULL);
		SetRetCode(TextOK);
	}
}
void TText::InsNextSection(string s) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else {
		TStr buf;
		strcpy_s(buf, s.c_str());
		pCurrent->pNext = new TTextLink(buf, NULL, pCurrent->pNext);
		SetRetCode(TextOK);
	}
}

void TText::DelDownLine(void) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else if (pCurrent->pDown == NULL)
		SetRetCode(TextNoDown);
	else if (pCurrent->pDown->IsAtom())
		pCurrent->pDown = pCurrent->pDown->pNext;
}

void TText::DelDownSection(void) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else if (pCurrent->pDown == NULL)
		SetRetCode(TextNoDown);
	else
		pCurrent->pDown = NULL;
}

void TText::DelNextLine(void) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else if (pCurrent->pNext == NULL)
		SetRetCode(TextNoNext);
	else if (pCurrent->pNext->IsAtom())
		pCurrent->pNext = pCurrent->pNext->pNext;
}

void TText::DelNextSection(void) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else if (pCurrent->pNext == NULL)
		SetRetCode(TextNoNext);
	else
		pCurrent->pNext = pCurrent->pNext->pNext;
}

//��������
int TText::Reset(void) {
	while (!St.empty())
		St.pop();
	pCurrent = pFirst;
	if (pCurrent != NULL) {
		St.push(pCurrent);
		if (pCurrent->pNext != NULL)
			St.push(pCurrent->pNext);
		if (pCurrent->pDown != NULL)
			St.push(pCurrent->pDown);
	}
	return IsTextEnded();
}

int TText::IsTextEnded(void) const {
	return !St.size();
}

int TText::GoNext(void) {
	if (!IsTextEnded()) {
		pCurrent = St.top(); St.pop();
		if (pCurrent != pFirst) {
			if (pCurrent->pNext != NULL)
				St.push(pCurrent->pNext);
			if (pCurrent->pDown != NULL)
				St.push(pCurrent->pDown);
		}
	}
	return IsTextEnded();
}

//����������� ������
PTTextLink TText::GetFirstAtom(PTTextLink pl) {
	PTTextLink tmp = pl;
	while (!tmp->IsAtom()) {
		St.push(tmp);
		tmp = tmp->GetDown();
	}
	return tmp;
}

//������
void TText::Print() {
	TextLevel = 0;
	PrintText(pFirst);
}

void TText::PrintText(PTTextLink ptl) {
	if (ptl != NULL) {
		for (int i = 0; i < TextLevel; i++)
			cout << " ";
		cout << ptl->Str << endl;
		TextLevel++; PrintText(ptl->GetDown());
		TextLevel--; PrintText(ptl->GetNext());
	}
}

void TText::PrintTextFile(PTTextLink ptl, ofstream & TxtFile)
{
	if (ptl != NULL) {
		for (int i = 0; i < TextLevel; i++)
			TxtFile << " ";
		TxtFile << ptl->Str << endl;
		TextLevel++; PrintTextFile(ptl->GetDown(), TxtFile);
		TextLevel--; PrintTextFile(ptl->GetNext(), TxtFile);
	}
}

PTText TText::GetCopy()
{
	PTTextLink pl1, pl2 = NULL, pl = pFirst, cpl = NULL;

	if (pFirst != NULL) {
		while (!St.empty()) St.pop();
		while (true)
		{
			if (pl != NULL) {									// ������� � ������� �����
				pl = GetFirstAtom(pl);
				St.push(pl);
				pl = pl->GetDown();
			}
			else if (St.empty()) break;
			else {
				pl1 = St.top(); St.pop();
				if (strstr(pl1->Str, "Copy") == NULL) {			// ������ ����, �������� ����� pDown �� ��� ������������� ����������
					pl2 = new TTextLink("Copy", pl1, cpl);
					St.push(pl2);
					pl = pl1->GetNext();
					cpl = NULL;
				}
				else {											// ������ ����
					strcpy_s(pl1->Str, pl1->pNext->Str);
					pl1->pNext = cpl;
					cpl = pl1;
				}
			}
		}
	}

	return new TText(cpl);
}


//������
void TText::Read(string FileName) {
	ifstream TxtFile(FileName);
	TextLevel = 0;
    if (TxtFile.is_open())
        pFirst = ReadText(TxtFile);
    else
        throw "Cant open the file";
}

PTTextLink TText::ReadText(ifstream& TxtFile)
{
	string buf;
	PTTextLink ptl = new TTextLink();
	PTTextLink tmp = ptl;
	while (!TxtFile.eof())
	{
		getline(TxtFile, buf);
		if (buf.size() > 0 && buf.front() == '}')
			break;
		else if (buf.size() > 0 && buf.front() == '{')
			ptl->pDown = ReadText(TxtFile);
		else
		{
			ptl->pNext = new TTextLink(buf.c_str());
			ptl = ptl->pNext;
		}
	}
	ptl = tmp;
	if (tmp->pDown == NULL)
	{
		tmp = tmp->pNext;
		delete ptl;
	}
	return tmp;
}

void TText::Write(string FileName)
{
	TextLevel = 0;
	ofstream TextFile(FileName);
	PrintTextFile(pFirst, TextFile);
}