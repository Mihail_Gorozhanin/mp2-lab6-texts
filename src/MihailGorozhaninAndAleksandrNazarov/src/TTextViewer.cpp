#pragma once

#include "../include/TTextViewer.h"

void TTextViewer::PushMessage(string message) {

    arg = MessageToArgs(message);

    if (arg.size() > 0 && arg[0] == "quit" || arg[0] == "q" || arg[0] == "exit")
        cout << "goodbye" << endl;
    else if (arg.size() > 0 && arg[0] == "help")
        Help();
    else if (arg.size() > 0 && arg[0] == "save" && arg.size() == 2) {
        Text.Write(arg[1]);
        FilePath = arg[1];
    }
    else if (arg.size() > 0 && arg[0] == "save" && arg.size() == 1)
        Save();
    else if (arg.size() > 0 && arg[0] == "load" && arg.size() == 2) {
        Text.Read(arg[1]);
        FilePath = arg[1];
    }
    else if (arg.size() > 0 && arg[0] == "insert" && arg.size() >= 3)
        Insert(arg[arg.size() - 1]);
    else if (arg.size() > 0 && arg[0] == "insertsection" && arg.size() >= 3)
        InsertLevel(arg[arg.size() - 1]);
    else if (arg.size() > 0 && arg[0] == "deletesection" && arg.size() >= 2)
        DeleteSection();
    else if (arg.size() > 0 && arg[0] == "replace" && arg.size() >= 3)
        Replace(arg[arg.size() - 1]);
    else if (arg.size() > 0 && arg[0] == "delete" && arg.size() >= 2)
        Delete();
    else if (arg.size() > 0 && arg[0] == "show")
        Text.Print();
    else if (arg.size() == 0)
        throw "you dont write any command";
    else
        throw "incorrect command or arguments, write help to show abilities of programm";

}

void TTextViewer::Insert(string str) {
    Navigation();
    Text.InsNextLine(str);
}


void TTextViewer::Replace(string str) {
    Navigation();
    Text.SetLine(str);
}

void TTextViewer::Delete() {
    Navigation();
    Text.GoPrevLink();
    Text.DelNextLine();
}

void TTextViewer::InsertLevel(string str) {
    Navigation();
    Text.InsDownSection(str);
}

void TTextViewer::DeleteSection() {
    Navigation();
    Text.DelDownSection();
}

void TTextViewer::Save() {
    if (FilePath != "")
        Text.Write(FilePath.data());
    else
        throw "where to save?";
}

int TTextViewer::Navigation() {
    Text.Reset();

    for (int i = 1; i < arg.size() - 1; i++) {
        for (int f = 0; f < StrToInt(arg[i]); f++) {
            if (Text.GoNextLink())
                throw "error of navigation";
        }
        if ((i + 1) != arg.size() - 1)
            if (Text.GoDownLink())
                throw "error of navigation";
    }
}

vector<string> TTextViewer::MessageToArgs(string message) {
    vector<string> result;
    bool IsIntoString = false;
    string buffstring = "";

    for (int i = 0; i < message.size(); i++) {
        if (message.at(i) == ' ' && !IsIntoString && buffstring.size() > 0) {
            result.push_back(buffstring);
            buffstring.clear();
        }
        else if (message.at(i) == '"')
            IsIntoString = !IsIntoString;
        else if (message.at(i) != ' ' || IsIntoString)
            buffstring.push_back(message.at(i));
    }
    if (buffstring.size() > 0)
        result.push_back(buffstring);

    return result;
}

int TTextViewer::StrToInt(string str) {
    int result = 0;

    for (int i = str.size() - 1, f = 0; i >= 0; i--, f++)
        if (str.at(i) <= '9' && str.at(i) >= '0')
            result = (str.at(i) - '0') * pow(10, f);
        else
            throw ("error: incorrect char of one of argument: " + str.at(i));

    return result;
}

void TTextViewer::Help() {
    cout << "commands: " << endl;
    cout << "save, save \"path\", load \"path\" - save or load text" << endl;
    cout << "<NavigationArguments> means arguments of the form: <NumOfString1> <NumOfString2> ... <NumOfStringN>" << endl;
    cout << "<NumOfString1> is mandatory argument, and other is optional arguments" << endl;
    cout << "if you write more than one <NumOfString> argument, you mean down to section" << endl;
    cout << "insert/replace <NavigationArguments> \"string\" - insert or replace string" << endl;
    cout << "insertsection <NumOfString> \"string\" - insert level with string" << endl;
    cout << "delete <NumOfString> - delete string" << endl;
    cout << "show - print text on console" << endl;
    cout << "exit, quit, q - quit the program" << endl;
    cout << "Credits: " << endl;
    cout << "Text editor by Aleksandr Nazarov and Mihail Gorozhanin" << endl;
}